<?php

namespace Drupal\Tests\ckeditor_fixed_toolbar\FunctionalJavascript;

use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\editor\Entity\Editor;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\filter\Entity\FilterFormat;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\node\Entity\NodeType;

/**
 * Tests the integration of CKEditor Fixed Toolbar.
 *
 * @group ckeditor_fixed_toolbar
 */
class FixedToolbarIntegrationTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The account.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $account;

  /**
   * The FilterFormat config entity used for testing.
   *
   * @var \Drupal\filter\FilterFormatInterface
   */
  protected $filterFormat;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'toolbar',
    'node',
    'ckeditor',
    'filter',
    'ckeditor_test',
    'ckeditor_fixed_toolbar',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create a text format and associate CKEditor.
    $this->filterFormat = FilterFormat::create([
      'format' => 'filtered_html',
      'name' => 'Filtered HTML',
      'weight' => 0,
    ]);
    $this->filterFormat->save();

    Editor::create([
      'format' => 'filtered_html',
      'editor' => 'ckeditor',
    ])->save();

    // Create a node type for testing.
    NodeType::create(['type' => 'page', 'name' => 'page'])->save();

    $field_storage = FieldStorageConfig::loadByName('node', 'body');

    // Create a body field instance for the 'page' node type.
    FieldConfig::create([
      'field_storage' => $field_storage,
      'bundle' => 'page',
      'label' => 'Body',
      'settings' => ['display_summary' => TRUE],
      'required' => TRUE,
    ])->save();

    // Assign widget settings for the 'default' form mode.
    EntityFormDisplay::create([
      'targetEntityType' => 'node',
      'bundle' => 'page',
      'mode' => 'default',
      'status' => TRUE,
    ])->setComponent('body', ['type' => 'text_textarea_with_summary'])
      ->save();

    $this->account = $this->drupalCreateUser([
      'access toolbar',
      'administer nodes',
      'create page content',
      'use text format filtered_html',
    ]);
    $this->drupalLogin($this->account);
  }

  /**
   * Tests if the CKEditor fixed toolbar works as expected.
   */
  public function testFixedToolbar() {
    $session = $this->getSession();
    $web_assert = $this->assertSession();

    $this->drupalGet('node/add/page');
    $session->getPage();

    // Asserts the CKEditor toolbar is present.
    $fixedEditorToolbar = $web_assert->elementExists('css', '#cke_edit-body-0-value .cke_top');
    $this->assertTrue($fixedEditorToolbar->hasAttribute('style'), 'CKEditor toolbar has no style attribute.');

    // Get style attribute and calculated top position.
    $styleAttr = $fixedEditorToolbar->getAttribute('style');
    $topPosition = $session->evaluateScript('return document.body.style.paddingTop;');

    // Asserts to check style attribute contains required CSS styles.
    $this->assertStringContainsString('position: sticky;', $styleAttr, 'CKEditor toolbar has no position: sticky style.');
    $this->assertStringContainsString('top: ' . $topPosition . ';', $styleAttr, '');
    $this->assertStringContainsString('z-index: 1;', $styleAttr, 'CKEditor toolbar has no z-index: 1 style.');
  }

}
