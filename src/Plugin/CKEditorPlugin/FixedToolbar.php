<?php

namespace Drupal\ckeditor_fixed_toolbar\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\ckeditor\CKEditorPluginContextualInterface;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "fixed_toolbar" plugin.
 *
 * @CKEditorPlugin(
 *   id = "fixed_toolbar",
 *   label = @Translation("Fixed Toolbar"),
 *   module = "ckeditor_fixed_toolbar"
 * )
 */
class FixedToolbar extends CKEditorPluginBase implements CKEditorPluginContextualInterface {

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return \Drupal::service('extension.list.module')->getPath('ckeditor_fixed_toolbar') . '/js/plugins/fixed_toolbar/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [];
  }

  /**
   * Checks if this plugin should be enabled based on the editor configuration.
   *
   * The editor's settings can be retrieved via $editor->getSettings().
   *
   * @param \Drupal\editor\Entity\Editor $editor
   *   A configured text editor object.
   *
   * @return bool
   *   Return TRUE when this plugin is enabled.
   */
  public function isEnabled(Editor $editor) {
    return TRUE;
  }

}
