CKEDITOR.plugins.add('fixed_toolbar', {
  init: function (editor) {
    CKEDITOR.on('instanceReady', function (e) {
      if (null === e.editor.container.find('.cke_top').getItem(0)) {
        return;
      }
      const toolBar = e.editor.container.find('.cke_top').getItem(0).$;
      toolBar.style.position = 'sticky';
      // We use the same offset as Drupal uses for body to make up for Admin Toolbar.
      toolBar.style.top = document.body.style.paddingTop;
      toolBar.style.zIndex = '1';
    });
  },
});
