# CKEditor: Fixed Toolbar

Project page: https://drupal.org/project/ckeditor_fixed_toolbar


## Introduction

The CKEditor Fixed Toolbar module provide a JS plugin to have a fixed CKEditor toolbar in textarea fields.


## Requirements

- CKEditor module from Drupal core


## Installing

__Versions 8.x-1.x:__ Install as usual, see the [official documentation](https://www.drupal.org/documentation/install/modules-themes/modules-8)
for further information.


## Configuration

- Only enable this module to activate fixed toolbar for textareas with CKEditor.


## Support

File bugs, feature requests and support requests in the [Drupal.org issue queue
of this project](https://www.drupal.org/project/issues/ckeditor_fixed_toolbar).


## Maintainers

Current maintainers for CKEditor Fixed Toolbar:
- [IT-Cru](https://www.drupal.org/u/IT-Cru)
- [mgoedecke](https://www.drupal.org/u/mgoedecke)
